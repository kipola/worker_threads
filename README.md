# Worker Threads

This project is [hosted on GitLab](https://gitlab.com/kipola/worker_threads)

The requirements were the following:

* Implement at least two distinct workers (e.g. computing some math function for
  a long time, writing and reading large files etc.).
* A worker thread must be safely paused, restarted, stopped and destroyed.
* The program should accept the following arguments:
    - `./program --help` prints help message and instructions
    - `./program --threads <nb_threads_to_run>` starts <nb_threads_to_run>
      threads and wait for instructions result: each thread should have an id
      in the range [1, <nb_threads_to_run>]
* Once the threads are started, the program should read instructions from the
  standard input.
  These instructions should have the following format:
    - `--pause <thread_id>` pauses the thread with the given id and print a
      confirmation message.
    - `--restart <thread_id>` restarts the thread with the given id (if paused)
      and print a confirmation message.
    - `--stop <thread_id>` stops the thread with the given id (if not stopped) and
      print a confirmation message.
    - `--status` prints the id, the status (`paused`, `running`, `stopped`,
      `finished`) and the current processing step for each thread.
* The program should exit gracefully when all the worker threads are finished.
  Invalid program options and instructions should be signaled.

## Dependencies

* [Boost](http://www.boost.org) (specifically `program_options` module).
  License: [Boost Software License](http://www.boost.org/LICENSE_1_0.txt)
* [Googletest](https://github.com/google/googletest) (optional): available as a submodule in 3rd_party/googletest.
  License: [BSD-3](https://github.com/google/googletest/blob/master/LICENSE)

The implementation of the _Threads Pool_ has no dependencies other than C++11 support.
_Boost_ is required for the example application and _Googletest_ is required in order to run the unit tests.

## Usage

Compile the lib (just the `ThreadPool` class), the unit test (if googletest
submodule was initialized) and the example executable `wk_ex_worker_thread` with
CMake:

```
mkdir build && cd build
cmake ..
make
```

If the tests are enabled, it is possible to run the unit tests with:

```
make test
```

The executable `wk_ex_worker_thread` is compiled in `build/bin/` and can be run
with:

```
./bin/wk_ex_worker_thread
```

## Comments

The focus is on the `ThreadPool` class.
It implements a thread pool with priority levels, using a Singleton pattern.
It takes advantage of C++11 features related to threads (`std::thread`, `std::condition_variable`, `std::mutex`, `std::future` and `std::lock`) in order not to use any external dependency.

### Potential extensions

#### Unit testing a singleton

Unit testing a singleton is a bit tricky, as different unit tests are not
independent anymore.
One potential (simple) solution would be to use the _pointer to implementation (Pimpl)_ idiom in order to separate the thread pool implementation from the Singleton mechanism.
That way, the implementation can be unit tested easily (without the singleton mechanism).

#### Function factory

In the small example `wk_ex_worker_thread`, a thread pool is instanciated and functions can be added to that pool.

Ideally, and depending on the exact use, the user should be able to write its own functions (as lambda functions, static class member methods, free functions or even non-static class member methods).
Using a factory pattern, it would be possible for the executable to be totally agnostic of the available functions, and the functions agnostic of the executable.
The only requirement is that each function is registered to the factory (which could be a singleton) and that the executable can access that factory.
See the branch `specific-functions-factory` for some exploratory work in that direction... Caution: this branch is work in progress and might not even compile!
