include(${PROJECT_SOURCE_DIR}/cmake/wk_utils.cmake)

###############################################################################
# Add an option to build a subsystem or not.
# _var The name of the variable to store the option in.
# _name The name of the option's target subsystem.
# _desc The description of the subsystem.
# _default The default value (TRUE or FALSE)
# ARGV5 The reason for disabling if the default is FALSE.
macro(WK_SUBSYS_OPTION _var _name _desc _default)
    set(_opt_name "BUILD_${_name}")
    WK_GET_SUBSYS_HYPERSTATUS(subsys_status ${_name})
    if(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
      option(${_opt_name} ${_desc} ${_default})
      if((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
        set(${_var} FALSE)
        if(${ARGC} GREATER 4)
          set(_reason ${ARGV4})
        else(${ARGC} GREATER 4)
          set(_reason "Disabled by default.")
        endif(${ARGC} GREATER 4)
        WK_SET_SUBSYS_STATUS(${_name} FALSE ${_reason})
        WK_DISABLE_DEPENDIES(${_name})
      elseif(NOT ${_opt_name})
        set(${_var} FALSE)
        WK_SET_SUBSYS_STATUS(${_name} FALSE "Disabled manually.")
        WK_DISABLE_DEPENDIES(${_name})
      else(NOT ${_default} AND NOT ${_opt_name})
        set(${_var} TRUE)
        WK_SET_SUBSYS_STATUS(${_name} TRUE)
        WK_ENABLE_DEPENDIES(${_name})
      endif((NOT ${_default} AND NOT ${_opt_name}) OR ("${_default}" STREQUAL "AUTO_OFF"))
    endif(NOT ("${subsys_status}" STREQUAL "AUTO_OFF"))
    WK_ADD_SUBSYSTEM(${_name} ${_desc})
endmacro(WK_SUBSYS_OPTION)

###############################################################################
# Make one subsystem depend on one or more other subsystems, and disable it if
# they are not being built.
# _var The cumulative build variable. This will be set to FALSE if the
#   dependencies are not met.
# _name The name of the subsystem.
# ARGN The subsystems and external libraries to depend on.
macro(WK_SUBSYS_DEPEND _var _name)
    set(options)
    set(oneValueArgs)
    set(multiValueArgs DEPS EXT_DEPS OPT_DEPS)
    cmake_parse_arguments(SUBSYS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    if(SUBSYS_DEPS)
        SET_IN_GLOBAL_MAP(WK_SUBSYS_DEPS ${_name} "${SUBSYS_DEPS}")
    endif(SUBSYS_DEPS)
    if(SUBSYS_EXT_DEPS)
        SET_IN_GLOBAL_MAP(WK_SUBSYS_EXT_DEPS ${_name} "${SUBSYS_EXT_DEPS}")
    endif(SUBSYS_EXT_DEPS)
    if(SUBSYS_OPT_DEPS)
        SET_IN_GLOBAL_MAP(WK_SUBSYS_OPT_DEPS ${_name} "${SUBSYS_OPT_DEPS}")
    endif(SUBSYS_OPT_DEPS)
    GET_IN_MAP(subsys_status WK_SUBSYS_HYPERSTATUS ${_name})
    if(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
        if(SUBSYS_DEPS)
        foreach(_dep ${SUBSYS_DEPS})
            WK_GET_SUBSYS_STATUS(_status ${_dep})
            if(NOT _status)
                set(${_var} FALSE)
                WK_SET_SUBSYS_STATUS(${_name} FALSE "Requires ${_dep}.")
            else(NOT _status)
                WK_GET_SUBSYS_INCLUDE_DIR(_include_dir ${_dep})
                STRING(REGEX MATCH "(.?cuda_)" match ${_include_dir})
                IF(match)
                    STRING(REGEX REPLACE "^([^_]*)\\_(.*)" "\\1/\\2" _cuda_include_dir ${_include_dir})
                    include_directories(${PROJECT_SOURCE_DIR}/modules/${_cuda_include_dir}/include)
                    cuda_include_directories(${PROJECT_SOURCE_DIR}/modules/${_cuda_include_dir}/include)
                ELSE(match)
                    include_directories(${PROJECT_SOURCE_DIR}/modules/${_include_dir}/include)
                ENDIF(match)
            endif(NOT _status)
        endforeach(_dep)
        endif(SUBSYS_DEPS)
        if(SUBSYS_EXT_DEPS)
        foreach(_dep ${SUBSYS_EXT_DEPS})
            string(TOUPPER "${_dep}_found" EXT_DEP_FOUND)
            if(NOT ${EXT_DEP_FOUND} OR (NOT (${EXT_DEP_FOUND} STREQUAL "TRUE")))
                set(${_var} FALSE)
                WK_SET_SUBSYS_STATUS(${_name} FALSE "Requires external library ${_dep}.")
            endif(NOT ${EXT_DEP_FOUND} OR (NOT (${EXT_DEP_FOUND} STREQUAL "TRUE")))
        endforeach(_dep)
        endif(SUBSYS_EXT_DEPS)
    endif(${_var} AND (NOT ("${subsys_status}" STREQUAL "AUTO_OFF")))
endmacro(WK_SUBSYS_DEPEND)

###############################################################################
# Add a set of include files to install.
# _component The part of WK that the install files belong to.
# _subdir The sub-directory for these include files.
# ARGN The include files.
macro(WK_ADD_INCLUDES _component _subdir)
    install(FILES ${ARGN} DESTINATION ${INCLUDE_INSTALL_DIR}/${_subdir}
        COMPONENT wk_${_component})
endmacro(WK_ADD_INCLUDES)

###############################################################################
# Add a library target.
# _name The library name.
# _component The part of WK that this library belongs to.
# 
# ARGN:
#   FILES       The source files for the library.
#   LINK_WITH   List if library to link against
macro(WK_ADD_LIBRARY _name _component)
  # parse arguments
  set(options)
  set(oneValueArgs)
  set(multiValueArgs FILES LINK_WITH)
  cmake_parse_arguments(WK_ADD_LIBRARY "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  # Create libbrary
  add_library(${_name} ${WK_LIB_TYPE} ${WK_ADD_LIBRARY_FILES})
  # Add link
  TARGET_LINK_LIBRARIES(${_name} ${WK_ADD_LIBRARY_LINK_WITH})

  if((UNIX AND NOT ANDROID) OR MINGW)
    target_link_libraries(${_name} m)
  endif()

  if (MINGW)
    target_link_libraries(${_name} gomp)
  endif()
  if(MSVC)
    target_link_libraries(${_name} delayimp.lib)  # because delay load is enabled for openmp.dll
  endif()

  set_target_properties(${_name} PROPERTIES
    VERSION ${WK_VERSION}
    SOVERSION ${WK_MAJOR_VERSION}.${WK_MINOR_VERSION})

  install(TARGETS ${_name}
    RUNTIME DESTINATION ${BIN_INSTALL_DIR} COMPONENT wk_${_component}
    LIBRARY DESTINATION ${LIB_INSTALL_DIR} COMPONENT wk_${_component}
    ARCHIVE DESTINATION ${LIB_INSTALL_DIR} COMPONENT wk_${_component})
endmacro(WK_ADD_LIBRARY)

###############################################################################
# Add an executable target.
# _name The executable name.
# _component The part of WK that this library belongs to.
# ARGN the source files for the library.
macro(WK_ADD_EXECUTABLE _name _component)
  add_executable(${_name} ${ARGN})
  if(WIN32 AND MSVC)
    set_target_properties(${_name} PROPERTIES DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
                                              RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
  endif()

  set(WK_EXECUTABLES ${WK_EXECUTABLES} ${_name})
  install(TARGETS ${_name} RUNTIME DESTINATION ${BIN_INSTALL_DIR}
      COMPONENT wk_${_component})
endmacro(WK_ADD_EXECUTABLE)

###############################################################################
# Add a test target.
# _name The test name.
# _exename The exe name.
# ARGN :
#    FILES the source files for the test
#    ARGUMENTS Arguments for test executable
#    LINK_WITH link test executable with libraries
macro(WK_ADD_TEST _name _exename)
    set(options)
    set(oneValueArgs)
    set(multiValueArgs FILES ARGUMENTS WORKING_DIR LINK_WITH)
    cmake_parse_arguments(WK_ADD_TEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    add_executable(wk_ut_${_exename} ${WK_ADD_TEST_FILES})
    # Add google test framework if not already build
    IF(NOT TARGET gtest)
      SET(INSTALL_GTEST OFF)
      SET(INSTALL_GMOCK OFF)
      ADD_SUBDIRECTORY(${WK_SOURCE_DIR}/3rd_party/googletest ${WK_OUTPUT_3RDPARTY_LIB_DIR}/googletest)
      MARK_AS_ADVANCED(BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS INSTALL_GTEST INSTALL_GMOCK gmock_build_tests gtest_build_samples gtest_build_tests
                       gtest_disable_pthreads gtest_force_shared_crt gtest_hide_internal_symbols)
    ENDIF(NOT TARGET gtest)
    # Add include location for testing framework
    include_directories(${WK_SOURCE_DIR}/3rd_party/googletest/googletest/include)
    include_directories(${WK_SOURCE_DIR}/3rd_party/googletest/googlemock/include)
    # Link extra library
    target_link_libraries(wk_ut_${_exename} ${WK_ADD_TEST_LINK_WITH} gtest gmock ${CLANG_LIBRARIES})
    # Only link if needed
    if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
      if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        set_target_properties(wk_ut_${_exename} PROPERTIES LINK_FLAGS -Wl)
      endif()
      target_link_libraries(wk_ut_${_exename} pthread)
    elseif(UNIX AND NOT ANDROID)
      set_target_properties(wk_ut_${_exename} PROPERTIES LINK_FLAGS -Wl,--as-needed)
      # GTest >= 1.5 requires pthread and CMake's 2.8.4 FindGTest is broken
      target_link_libraries(wk_ut_${_exename} pthread)
    elseif(CMAKE_COMPILER_IS_GNUCXX AND MINGW)
      set_target_properties(wk_ut_${_exename} PROPERTIES LINK_FLAGS "-Wl,--allow-multiple-definition -Wl,--as-needed")
    elseif(WIN32)
      set_target_properties(wk_ut_${_exename} PROPERTIES LINK_FLAGS_RELEASE /OPT:REF)
    endif()
    # Register new text
    add_test(NAME ${_name} COMMAND wk_ut_${_exename} ${WK_ADD_TEST_ARGUMENTS} WORKING_DIRECTORY ${WK_ADD_TEST_WORKING_DIR})
endmacro(WK_ADD_TEST)

###############################################################################
# Add an example target.
# _name The example name.
# ARGN :
#    FILES the source files for the example
#    LINK_WITH link example executable with libraries
macro(WK_ADD_EXAMPLE _name)
    set(options)
    set(oneValueArgs)
    set(multiValueArgs FILES LINK_WITH)
    cmake_parse_arguments(WK_ADD_EXAMPLE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    add_executable(wk_ex_${_name} ${WK_ADD_EXAMPLE_FILES})
    target_link_libraries(wk_ex_${_name} ${WK_ADD_EXAMPLE_LINK_WITH} ${CLANG_LIBRARIES})
    if(WIN32 AND MSVC)
      set_target_properties(wk_ex_${_name} PROPERTIES DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
                                                           RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
    endif(WIN32 AND MSVC)
endmacro(WK_ADD_EXAMPLE)

###############################################################################
# Add compile flags to a target (because CMake doesn't provide something so
# common itself).
# _name The target name.
# _flags The new compile flags to be added, as a string.
macro(WK_ADD_CFLAGS _name _flags)
    get_target_property(_current_flags ${_name} COMPILE_FLAGS)
    if(NOT _current_flags)
        set_target_properties(${_name} PROPERTIES COMPILE_FLAGS ${_flags})
    else(NOT _current_flags)
        set_target_properties(${_name} PROPERTIES
            COMPILE_FLAGS "${_current_flags} ${_flags}")
    endif(NOT _current_flags)
endmacro(WK_ADD_CFLAGS)


###############################################################################
# Add link flags to a target (because CMake doesn't provide something so
# common itself).
# _name The target name.
# _flags The new link flags to be added, as a string.
macro(WK_ADD_LINKFLAGS _name _flags)
    get_target_property(_current_flags ${_name} LINK_FLAGS)
    if(NOT _current_flags)
        set_target_properties(${_name} PROPERTIES LINK_FLAGS ${_flags})
    else(NOT _current_flags)
        set_target_properties(${_name} PROPERTIES
            LINK_FLAGS "${_current_flags} ${_flags}")
    endif(NOT _current_flags)
endmacro(WK_ADD_LINKFLAGS)

###############################################################################
# PRIVATE

###############################################################################
# Reset the subsystem status map.
macro(WK_RESET_MAPS)
    foreach(_ss ${WK_SUBSYSTEMS})
        string(TOUPPER "WK_${_ss}_SUBSYS" WK_SUBSYS_SUBSYS)
	if (${WK_SUBSYS_SUBSYS})
            string(TOUPPER "WK_${_ss}_SUBSYS_DESC" WK_PARENT_SUBSYS_DESC)
	    set(${WK_SUBSYS_SUBSYS_DESC} "" CACHE INTERNAL "" FORCE)
	    set(${WK_SUBSYS_SUBSYS} "" CACHE INTERNAL "" FORCE)
	endif (${WK_SUBSYS_SUBSYS})
    endforeach(_ss)

    set(WK_SUBSYS_HYPERSTATUS "" CACHE INTERNAL
        "To Build Or Not To Build, That Is The Question." FORCE)
    set(WK_SUBSYS_STATUS "" CACHE INTERNAL
        "To build or not to build, that is the question." FORCE)
    set(WK_SUBSYS_REASONS "" CACHE INTERNAL
        "But why?" FORCE)
    set(WK_SUBSYS_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
    set(WK_SUBSYS_EXT_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
    set(WK_SUBSYS_OPT_DEPS "" CACHE INTERNAL "A depends on B and C." FORCE)
    set(WK_SUBSYSTEMS "" CACHE INTERNAL "Internal list of subsystems" FORCE)
    set(WK_SUBSYS_DESC "" CACHE INTERNAL "Subsystem descriptions" FORCE)
endmacro(WK_RESET_MAPS)


###############################################################################
# Register a subsystem.
# _name Subsystem name.
# _desc Description of the subsystem
macro(WK_ADD_SUBSYSTEM _name _desc)
    set(_temp ${WK_SUBSYSTEMS})
    list(APPEND _temp ${_name})
    set(WK_SUBSYSTEMS ${_temp} CACHE INTERNAL "Internal list of subsystems"
        FORCE)
    SET_IN_GLOBAL_MAP(WK_SUBSYS_DESC ${_name} ${_desc})
endmacro(WK_ADD_SUBSYSTEM)

###############################################################################
# Register a subsubsystem.
# _name Subsystem name.
# _desc Description of the subsystem
macro(WK_ADD_SUBSUBSYSTEM _parent _name _desc)
  string(TOUPPER "WK_${_parent}_SUBSYS" WK_PARENT_SUBSYS)
  string(TOUPPER "WK_${_parent}_SUBSYS_DESC" WK_PARENT_SUBSYS_DESC)
  set(_temp ${${WK_PARENT_SUBSYS}})
  list(APPEND _temp ${_name})
  set(${WK_PARENT_SUBSYS} ${_temp} CACHE INTERNAL "Internal list of ${_parenr} subsystems"
    FORCE)
  set_in_global_map(${WK_PARENT_SUBSYS_DESC} ${_name} ${_desc})
endmacro(WK_ADD_SUBSUBSYSTEM)


###############################################################################
# Set the status of a subsystem.
# _name Subsystem name.
# _status TRUE if being built, FALSE otherwise.
# ARGN[0] Reason for not building.
macro(WK_SET_SUBSYS_STATUS _name _status)
    if(${ARGC} EQUAL 3)
        set(_reason ${ARGV2})
    else(${ARGC} EQUAL 3)
        set(_reason "No reason")
    endif(${ARGC} EQUAL 3)
    SET_IN_GLOBAL_MAP(WK_SUBSYS_STATUS ${_name} ${_status})
    SET_IN_GLOBAL_MAP(WK_SUBSYS_REASONS ${_name} ${_reason})
endmacro(WK_SET_SUBSYS_STATUS)

###############################################################################
# Set the status of a subsystem.
# _name Subsystem name.
# _status TRUE if being built, FALSE otherwise.
# ARGN[0] Reason for not building.
macro(WK_SET_SUBSUBSYS_STATUS _parent _name _status)
    if(${ARGC} EQUAL 4)
        set(_reason ${ARGV2})
    else(${ARGC} EQUAL 4)
        set(_reason "No reason")
    endif(${ARGC} EQUAL 4)
    SET_IN_GLOBAL_MAP(WK_SUBSYS_STATUS ${_parent}_${_name} ${_status})
    SET_IN_GLOBAL_MAP(WK_SUBSYS_REASONS ${_parent}_${_name} ${_reason})
endmacro(WK_SET_SUBSUBSYS_STATUS)


###############################################################################
# Get the status of a subsystem
# _var Destination variable.
# _name Name of the subsystem.
macro(WK_GET_SUBSYS_STATUS _var _name)
    GET_IN_MAP(${_var} WK_SUBSYS_STATUS ${_name})
endmacro(WK_GET_SUBSYS_STATUS)

###############################################################################
# Get the status of a subsystem
# _var Destination variable.
# _name Name of the subsystem.
macro(WK_GET_SUBSUBSYS_STATUS _var _parent _name)
    GET_IN_MAP(${_var} WK_SUBSYS_STATUS ${_parent}_${_name})
endmacro(WK_GET_SUBSUBSYS_STATUS)


###############################################################################
# Set the hyperstatus of a subsystem and its dependee
# _name Subsystem name.
# _dependee Dependant subsystem.
# _status AUTO_OFF to disable AUTO_ON to enable
# ARGN[0] Reason for not building.
macro(WK_SET_SUBSYS_HYPERSTATUS _name _dependee _status)
    SET_IN_GLOBAL_MAP(WK_SUBSYS_HYPERSTATUS ${_name}_${_dependee} ${_status})
    if(${ARGC} EQUAL 4)
        SET_IN_GLOBAL_MAP(WK_SUBSYS_REASONS ${_dependee} ${ARGV3})
    endif(${ARGC} EQUAL 4)
endmacro(WK_SET_SUBSYS_HYPERSTATUS)

###############################################################################
# Get the hyperstatus of a subsystem and its dependee
# _name IN subsystem name.
# _dependee IN dependant subsystem.
# _var OUT hyperstatus
# ARGN[0] Reason for not building.
macro(WK_GET_SUBSYS_HYPERSTATUS _var _name)
    set(${_var} "AUTO_ON")
    if(${ARGC} EQUAL 3)
        GET_IN_MAP(${_var} WK_SUBSYS_HYPERSTATUS ${_name}_${ARGV2})
    else(${ARGC} EQUAL 3)
        foreach(subsys ${WK_SUBSYS_DEPS_${_name}})
            if("${WK_SUBSYS_HYPERSTATUS_${subsys}_${_name}}" STREQUAL "AUTO_OFF")
                set(${_var} "AUTO_OFF")
                break()
            endif("${WK_SUBSYS_HYPERSTATUS_${subsys}_${_name}}" STREQUAL "AUTO_OFF")
        endforeach(subsys)
    endif(${ARGC} EQUAL 3)
endmacro(WK_GET_SUBSYS_HYPERSTATUS)

###############################################################################
# Set the hyperstatus of a subsystem and its dependee
macro(WK_UNSET_SUBSYS_HYPERSTATUS _name _dependee)
    unset(WK_SUBSYS_HYPERSTATUS_${_name}_${dependee})
endmacro(WK_UNSET_SUBSYS_HYPERSTATUS)

###############################################################################
# Set the include directory name of a subsystem.
# _name Subsystem name.
# _includedir Name of subdirectory for includes
# ARGN[0] Reason for not building.
macro(WK_SET_SUBSYS_INCLUDE_DIR _name _includedir)
    SET_IN_GLOBAL_MAP(WK_SUBSYS_INCLUDE ${_name} ${_includedir})
endmacro(WK_SET_SUBSYS_INCLUDE_DIR)


###############################################################################
# Get the include directory name of a subsystem - return _name if not set
# _var Destination variable.
# _name Name of the subsystem.
macro(WK_GET_SUBSYS_INCLUDE_DIR _var _name)
    GET_IN_MAP(${_var} WK_SUBSYS_INCLUDE ${_name})
    if(NOT ${_var})
      set (${_var} ${_name})
    endif(NOT ${_var})
endmacro(WK_GET_SUBSYS_INCLUDE_DIR)


###############################################################################
# Write a report on the build/not-build status of the subsystems
macro(WK_WRITE_STATUS_REPORT)
    message(STATUS "The following subsystems will be built:")
    foreach(_ss ${WK_SUBSYSTEMS})
        WK_GET_SUBSYS_STATUS(_status ${_ss})
        if(_status)
	    set(message_text "  ${_ss}")
	    string(TOUPPER "WK_${_ss}_SUBSYS" WK_SUBSYS_SUBSYS)
	    if (${WK_SUBSYS_SUBSYS})
	        set(will_build)
		foreach(_sub ${${WK_SUBSYS_SUBSYS}})
		    WK_GET_SUBSYS_STATUS(_sub_status ${_ss}_${_sub})
		    if (_sub_status)
		        set(will_build "${will_build}\n       |_ ${_sub}")
		    endif (_sub_status)
		endforeach(_sub)
		if (NOT ("${will_build}" STREQUAL ""))
		  set(message_text  "${message_text}\n       building: ${will_build}")
		endif (NOT ("${will_build}" STREQUAL ""))
		set(wont_build)
		foreach(_sub ${${WK_SUBSYS_SUBSYS}})
		    WK_GET_SUBSYS_STATUS(_sub_status ${_ss}_${_sub})
		    WK_GET_SUBSYS_HYPERSTATUS(_sub_hyper_status ${_ss}_${sub})
		    if (NOT _sub_status OR ("${_sub_hyper_status}" STREQUAL "AUTO_OFF"))
		        GET_IN_MAP(_reason WK_SUBSYS_REASONS ${_ss}_${_sub})
		        set(wont_build "${wont_build}\n       |_ ${_sub}: ${_reason}")
		    endif (NOT _sub_status OR ("${_sub_hyper_status}" STREQUAL "AUTO_OFF"))
		endforeach(_sub)
		if (NOT ("${wont_build}" STREQUAL ""))
		    set(message_text  "${message_text}\n       not building: ${wont_build}")
		endif (NOT ("${wont_build}" STREQUAL ""))
	    endif (${WK_SUBSYS_SUBSYS})
	    message(STATUS "${message_text}")
        endif(_status)
    endforeach(_ss)

    message(STATUS "The following subsystems will not be built:")
    foreach(_ss ${WK_SUBSYSTEMS})
        WK_GET_SUBSYS_STATUS(_status ${_ss})
        WK_GET_SUBSYS_HYPERSTATUS(_hyper_status ${_ss})
        if(NOT _status OR ("${_hyper_status}" STREQUAL "AUTO_OFF"))
            GET_IN_MAP(_reason WK_SUBSYS_REASONS ${_ss})
            message(STATUS "  ${_ss}: ${_reason}")
        endif(NOT _status OR ("${_hyper_status}" STREQUAL "AUTO_OFF"))
    endforeach(_ss)
endmacro(WK_WRITE_STATUS_REPORT)

##############################################################################
# Collect subdirectories from dirname that contains filename and store them in
#  varname.
# WARNING If extra arguments are given then they are considered as exception
# list and varname will contain subdirectories of dirname that contains
# fielename but doesn't belong to exception list.
# dirname IN parent directory
# filename IN file name to look for in each subdirectory of parent directory
# varname OUT list of subdirectories containing filename
# exception_list OPTIONAL and contains list of subdirectories not to account
macro(collect_subproject_directory_names dirname filename names dirs)
    file(GLOB globbed RELATIVE "${dirname}" "${dirname}/*/${filename}")
    if(${ARGC} GREATER 4)
        set(exclusion_list ${ARGN})
        foreach(file ${globbed})
            get_filename_component(dir ${file} PATH)
            list(FIND exclusion_list ${dir} excluded)
            if(excluded EQUAL -1)
                set(${dirs} ${${dirs}} ${dir})
            endif(excluded EQUAL -1)
        endforeach()
    else(${ARGC} GREATER 4)
        foreach(file ${globbed})
            get_filename_component(dir ${file} PATH)
            set(${dirs} ${${dirs}} ${dir})
        endforeach(file)
    endif(${ARGC} GREATER 4)
    foreach(subdir ${${dirs}})
        file(STRINGS ${dirname}/${subdir}/CMakeLists.txt name REGEX "[setSET ]+\\(.*SUBSYS_NAME .*\\)$")
        string(REGEX REPLACE "[setSET ]+\\(.*SUBSYS_NAME[ ]+([A-Za-z0-9_]+)[ ]*\\)" "\\1" name "${name}")
        set(${names} ${${names}} ${name})
        file(STRINGS ${dirname}/${subdir}/CMakeLists.txt DEPENDENCIES REGEX "set.*SUBSYS_DEPS .*\\)")
        string(REGEX REPLACE "set.*SUBSYS_DEPS" "" DEPENDENCIES "${DEPENDENCIES}")
        string(REPLACE ")" "" DEPENDENCIES "${DEPENDENCIES}")
        string(STRIP "${DEPENDENCIES}" DEPENDENCIES)
        string(REPLACE " " ";" DEPENDENCIES "${DEPENDENCIES}")
        if(NOT("${DEPENDENCIES}" STREQUAL ""))
            list(REMOVE_ITEM DEPENDENCIES "#")
            string(TOUPPER "WK_${name}_DEPENDS" SUBSYS_DEPENDS)
            set(${SUBSYS_DEPENDS} ${DEPENDENCIES})
            foreach(dependee ${DEPENDENCIES})
                string(TOUPPER "WK_${dependee}_DEPENDIES" SUBSYS_DEPENDIES)
                set(${SUBSYS_DEPENDIES} ${${SUBSYS_DEPENDIES}} ${name})
            endforeach(dependee)
        endif(NOT("${DEPENDENCIES}" STREQUAL ""))
    endforeach(subdir)
endmacro()

########################################################################################
# Macro to disable subsystem dependies
# _subsys IN subsystem name
macro(WK_DISABLE_DEPENDIES _subsys)
    string(TOUPPER "wk_${_subsys}_dependies" WK_SUBSYS_DEPENDIES)
    if(NOT ("${${WK_SUBSYS_DEPENDIES}}" STREQUAL ""))
        foreach(dep ${${WK_SUBSYS_DEPENDIES}})
            WK_SET_SUBSYS_HYPERSTATUS(${_subsys} ${dep} AUTO_OFF "Disabled: ${_subsys} missing.")
            set(BUILD_${dep} OFF CACHE BOOL "Disabled: ${_subsys} missing." FORCE)
        endforeach(dep)
    endif(NOT ("${${WK_SUBSYS_DEPENDIES}}" STREQUAL ""))
endmacro(WK_DISABLE_DEPENDIES subsys)

########################################################################################
# Macro to enable subsystem dependies
# _subsys IN subsystem name
macro(WK_ENABLE_DEPENDIES _subsys)
    string(TOUPPER "wk_${_subsys}_dependies" WK_SUBSYS_DEPENDIES)
    if(NOT ("${${WK_SUBSYS_DEPENDIES}}" STREQUAL ""))
        foreach(dep ${${WK_SUBSYS_DEPENDIES}})
            WK_GET_SUBSYS_HYPERSTATUS(dependee_status ${_subsys} ${dep})
            if("${dependee_status}" STREQUAL "AUTO_OFF")
                WK_SET_SUBSYS_HYPERSTATUS(${_subsys} ${dep} AUTO_ON)
                GET_IN_MAP(desc WK_SUBSYS_DESC ${dep})
                set(BUILD_${dep} ON CACHE BOOL "${desc}" FORCE)
            endif("${dependee_status}" STREQUAL "AUTO_OFF")
        endforeach(dep)
    endif(NOT ("${${WK_SUBSYS_DEPENDIES}}" STREQUAL ""))
endmacro(WK_ENABLE_DEPENDIES subsys)

########################################################################################
# Macro to build subsystem centric documentation
# _subsys IN the name of the subsystem to generate documentation for
macro (WK_ADD_DOC _subsys)
  string(TOUPPER "${_subsys}" SUBSYS)
  set(doc_subsys "doc_${_subsys}")
  GET_IN_MAP(dependencies WK_SUBSYS_DEPS ${_subsys})
  if(DOXYGEN_FOUND)
    if(HTML_HELP_COMPILER)
      set(DOCUMENTATION_HTML_HELP YES)
    else(HTML_HELP_COMPILER)
      set(DOCUMENTATION_HTML_HELP NO)
    endif(HTML_HELP_COMPILER)
    if(DOXYGEN_DOT_EXECUTABLE)
      set(HAVE_DOT YES)
    else(DOXYGEN_DOT_EXECUTABLE)
      set(HAVE_DOT NO)
    endif(DOXYGEN_DOT_EXECUTABLE)
    if(NOT "${dependencies}" STREQUAL "")
      set(STRIPPED_HEADERS "${WK_SOURCE_DIR}/${dependencies}/include")
      string(REPLACE ";" "/include \\\n\t\t\t\t\t\t\t\t\t\t\t\t ${WK_SOURCE_DIR}/"
             STRIPPED_HEADERS "${STRIPPED_HEADERS}")
    endif(NOT "${dependencies}" STREQUAL "")
    set(DOC_SOURCE_DIR "\"${CMAKE_CURRENT_SOURCE_DIR}\"\\")
    foreach(dep ${dependencies})
      set(DOC_SOURCE_DIR
          "${DOC_SOURCE_DIR}\n\t\t\t\t\t\t\t\t\t\t\t\t \"${WK_SOURCE_DIR}/${dep}\"\\")
    endforeach(dep)
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/html")
    set(doxyfile "${CMAKE_CURRENT_BINARY_DIR}/doxyfile")
    configure_file("${WK_SOURCE_DIR}/doc/doxygen/doxyfile.in" ${doxyfile})
    add_custom_target(${doc_subsys} ${DOXYGEN_EXECUTABLE} ${doxyfile})
    if(USE_PROJECT_FOLDERS)
      set_target_properties(${doc_subsys} PROPERTIES FOLDER "Documentation")
    endif(USE_PROJECT_FOLDERS)
  endif(DOXYGEN_FOUND)
endmacro(WK_ADD_DOC)
