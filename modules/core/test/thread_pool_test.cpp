//
//  thread_pool_test.cpp
//  WorkerThreads
//
//  Created by Gabriel Cuendet on 03.02.18.
//  Copyright © 2018 Gabriel Cuendet. All rights reserved.
//

#include <sstream>
#include <thread>
#include <chrono>

#include "gtest/gtest.h"

#include "wk/core/thread_pool.hpp"

size_t n_threads(0);

/**
 * @class   ThreadPoolTest
 * @brief   Testing unit for ThreadPool class
 */
class ThreadPoolTest : public ::testing::Test {

protected:
  // CAUTION: Fake construction/destruction of thread pool between each tests
  // by only setting starting all threads at the beginning and stopping them at
  // the end...
  void SetUp(void) {
    ThreadPool& wk_tp = ThreadPool::Get(n_threads);
    for (int i = 0; i < wk_tp.size(); ++i) {
      wk_tp.RestartThread(i);
    }
  }

  void TearDown(void) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    ThreadPool& wk_tp = ThreadPool::Get(n_threads);

    for (int i = 0; i < wk_tp.size(); ++i) {
      wk_tp.PauseThread(i);
    }
  }
};

TEST_F(ThreadPoolTest, ThreadPoolInstanciation) {
  EXPECT_TRUE(true);
}

TEST_F(ThreadPoolTest, ThreadStatus) {
  ThreadPool& wk_tp = ThreadPool::Get();

  for (size_t i = 0; i < n_threads; ++i) {
    std::cout << "Thread " << i << " is " << wk_tp.status(i) << std::endl;
    EXPECT_EQ(wk_tp.status(i), ThreadPool::ThreadState::kWaiting);
  }
}

TEST_F(ThreadPoolTest, ThreadPausedStatus) {
  ThreadPool& wk_tp = ThreadPool::Get();
  wk_tp.PauseThread(0);
  std::chrono::milliseconds duration(1);
  std::this_thread::sleep_for(duration);
  for (size_t i = 0; i < n_threads; ++i) {
    std::cout << "Thread " << i << " is " << wk_tp.status(i) << std::endl;
    if (i == 0) {
      EXPECT_EQ(wk_tp.status(i), ThreadPool::ThreadState::kPaused);
    } else {
      EXPECT_EQ(wk_tp.status(i), ThreadPool::ThreadState::kWaiting);
    }
  }
}

TEST_F(ThreadPoolTest, ThreadQueueLambda) {
  ThreadPool& wk_tp = ThreadPool::Get();

  struct Fibonacci {
    static int Compute(int n) {
      int a = 1;
      int b = 1;
      int res;
      for (int i = 0; i < n; ++i) {
        res = a+b;
        a = b;
        b = res;
      }
      return res;
    }
  };

  auto res = wk_tp.Enqueue(ThreadPool::TaskPriority::kLow, [](int n)->int {return n*n;}, 100);
  auto fibo = wk_tp.Enqueue(ThreadPool::TaskPriority::kHigh, Fibonacci::Compute, 100);

  EXPECT_EQ(fibo.get(), 1445263496);
  EXPECT_EQ(res.get(), 10000);
}

int main(int argc, const char * argv[]) {
  ::testing::InitGoogleTest(&argc, const_cast<char**>(argv));

  if (argc < 2) {
    std::cout << "usage: " << argv[0] << " n_threads" << std::endl;
    return -1;
  }

  std::istringstream ss(argv[1]);
  size_t x;
  if (!(ss >> x))
    std::cerr << "Invalid number " << argv[1] << std::endl;

  n_threads = x;
  return RUN_ALL_TESTS();
}
