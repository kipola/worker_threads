//
//  worker_threads_example.cpp
//  WorkerThreads
//
//  Created by Gabriel Cuendet on 09.02.18.
//  Copyright © 2018 Gabriel Cuendet. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <chrono>
#include <thread>

#include "boost/program_options.hpp"
#include "boost/tokenizer.hpp"
#include "boost/algorithm/string.hpp"

#include "wk/core/thread_pool.hpp"
#include "functions.hpp"

namespace po = boost::program_options;

struct App {

  App(size_t n) {
    ThreadPool& tp_ = ThreadPool::Get(n);
  };

  static void OnPause(int id) {
    ThreadPool& tp_ = ThreadPool::Get();
    if (id < 1 || id > tp_.size()) return;

    tp_.PauseThread(id - 1);
    std::chrono::milliseconds duration(1);
    std::this_thread::sleep_for(duration);

    ThreadPool::ThreadState state = tp_.status(id - 1);
    std::cout << "Thread " << id << " is " << state << std::endl;

    if (state == ThreadPool::ThreadState::kStopped) {
      std::cout << " and thus cannot be paused!" << std::endl;
    }
  }

  static void OnRestart(int id) {
    ThreadPool& tp_ = ThreadPool::Get();
    if (id < 1 || id > tp_.size()) return;

    tp_.RestartThread(id - 1);
    std::chrono::milliseconds duration(1);
    std::this_thread::sleep_for(duration);

    ThreadPool::ThreadState state = tp_.status(id - 1);
    std::cout << "Thread " << id << " is " << state << std::endl;

    if (state == ThreadPool::ThreadState::kStopped) {
      std::cout << " and thus cannot be restarted!" << std::endl;
    }
  }

  static void OnStop(int id) {
    ThreadPool& tp_ = ThreadPool::Get();
    if (id < 1 || id > tp_.size()) return;

    tp_.StopThread(id - 1);
    std::chrono::milliseconds duration(1);
    std::this_thread::sleep_for(duration);

    ThreadPool::ThreadState state = tp_.status(id - 1);
    std::cout << "Thread " << id << " is " << state << std::endl;
  }

  static void OnStatus(bool status) {
    if (!status) return;
    ThreadPool& tp_ = ThreadPool::Get();
    for (int i = 0; i < tp_.size(); ++i) {
      std::cout << "thread " << i + 1 << ": " << tp_.status(i) << std::endl;
    }
    std::cout << "Number of functions waiting in the pool: " << tp_.QueueSize() << std::endl;
  }

  static void OnAddPrimeFactors(long long n) {
    ThreadPool& tp_ = ThreadPool::Get();
    tp_.Enqueue(ThreadPool::TaskPriority::kNormal, &PrimeFactors, n);
    OnStatus(true);
  }

  static void OnWaitSeconds(int n) {
    ThreadPool& tp_ = ThreadPool::Get();
    tp_.Enqueue(ThreadPool::TaskPriority::kNormal, &WaitSeconds, n);
    OnStatus(true);
  }

  void Run() {
    // Setup options.
    po::options_description desc("Options");
    desc.add_options()
    ("pause,p",
     po::value<unsigned int>()->notifier(OnPause)->value_name("<id>"),
     "Pause the thread <id>")
    ("restart,r",
     po::value<unsigned int>()->notifier(OnRestart)->value_name("<id>"),
     "Restart the thread <id>")
    ("stop",
     po::value<unsigned int>()->notifier(OnStop)->value_name("<id>"),
     "Stop the thread <id>")
    ("status,s",
     po::bool_switch()->notifier(OnStatus),
     "Print the status of all the threads and the number of functions in the pool")
    ("help,h",
     "Print the help")
    ("PrimeFactors",
     po::value<long long>()->notifier(OnAddPrimeFactors),
     "Add the function PrimeFactors (computes the prime factors of n) to the ThreadPool")
    ("WaitSeconds",
     po::value<int>()->notifier(OnWaitSeconds),
     "Add the function WaitSeconds (make the current thread wait for n sec) to the ThreadPool");

    // When initially launched, print the help
    std::cout << desc << std::endl;

    std::string input;
    while (std::getline(std::cin, input)) {
      if (input == "exit" || input == "quit") return;
      boost::char_separator<char> sep(" \n\r");
      boost::tokenizer<boost::char_separator<char> > tok(input, sep);

      std::vector<std::string> str;
      copy(tok.begin(), tok.end(), back_inserter(str));

      // Parse mocked up input.
      po::variables_map vm;
      try {
        po::store(po::command_line_parser(str)
                .options(desc).run(), vm);
        po::notify(vm);
      } catch(std::exception& e) {
        std::cout << e.what() << std::endl << desc << std::endl;
      }

      if (vm.count("help")) {
        std::cout << desc << std::endl;
      }
    }
  };
};

int main(int argc, char** argv) {
  unsigned int n_threads;
  // Setup options.
  po::options_description desc("This program allows the user to control multiple worker threads and accepts the following arguments");
  desc.add_options()
  ("threads,t", po::value<unsigned int>(&n_threads)->value_name("<NUM>"), "Starts <NUM> threads and wait for instructions.\nEach thread has an id in the range [1, <NUM>]")
  ("help,h", "Print the help");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  } catch(std::exception& e) {
    std::cout << e.what() << std::endl << desc << std::endl;
    return -1;
  }

  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return 0;
  }

  if (!vm.count("threads")) {
    std::cout << "Please provide the number of threads to start" << std::endl <<
    desc << std::endl;
    return -1;
  }

  App app(n_threads);
  app.Run();

}
