//
//  functions.hpp
//  WorkerThreads
//
//  Created by Gabriel Cuendet on 11.02.18.
//  Copyright © 2018 Gabriel Cuendet. All rights reserved.
//

#ifndef functions_h
#define functions_h

void PrimeFactors(long long n) {
  long long z = 2;

  while (z * z <= n) {
    if (n % z == 0) {
      n /= z;
    } else {
      z++;
    }
  }
  if (n >= 1) {
    //
  }
}

void WaitSeconds(int n) {
  std::chrono::seconds duration(n);
  std::this_thread::sleep_for(duration);
}

#endif /* functions_h */
