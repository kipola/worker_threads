//
//  thread_pool.hpp
//  WorkerThreads
//
//  Created by Gabriel Cuendet on 03.02.18.
//  Copyright © 2018 Gabriel Cuendet. All rights reserved.
//

#ifndef thread_pool_hpp
#define thread_pool_hpp

#include <vector>
#include <iostream>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <functional>
#include <future>
#include <queue>
#include <deque>


class ThreadPool {
 public:

  /**
   *  @enum   TaskPriority
   *  @brief  Possible task's priority
   */
  enum class TaskPriority : char {
    /** Low priority */
    kLow = 0,
    /** Normal Priority */
    kNormal,
    /** High Priority */
    kHigh
  };

  /**
   *  @enum  ThreadState
   *  @brief Possible states of a thread
   */
  enum class ThreadState : char {
    /** Thread is stopped*/
    kStopped = 0,
    /** Thread is paused, but not stopped */
    kPaused,
    /** Thread is running but has nothing to do, so waiting */
    kWaiting,
    /** Thread is running */
    kRunning
  };

#pragma mark -
#pragma mark Initialization methods

  /**
   * @name Get
   * @fn static ThreadPool& Get(const size_t& n_threads)
   * @brief Accessor for the thread pool, imlpemented as a singleton
   * @param[in] n_threads  Number of threads to create in the pool, the first
   *            time it is get
   */
  static ThreadPool& Get(const size_t& n_threads = 1);

  /**
   *  @name ThreadPool
   *  @fn ThreadPool(const ThreadPool& other) = delete
   *  @brief Deleted copy constructor
   */
  ThreadPool(const ThreadPool& other) = delete;

  /**
   *  @name ThreadPool
   *  @fn ThreadPool(ThreadPool&& other) = delete
   *  @brief Deleted move constructor
   */
  ThreadPool(ThreadPool&& other) = delete;

  /**
   *  @name operator=
   *  @fn ThreadPool& operator=(const ThreadPool& other) = delete
   *  @brief Deleted assignement operator
   */
  ThreadPool& operator=(const ThreadPool& other) = delete;

  /**
   *  @name operator=
   *  @fn ThreadPool& operator=(ThreadPool&& other) = delete
   *  @brief Deleted move assignement operator
   */
  ThreadPool& operator=(ThreadPool&& other) = delete;

  /**
   * @name ~ThreadPool
   * @fn ~ThreadPool(void)
   * @brief Default destructor of the class
   */
  ~ThreadPool(void);

#pragma mark -
#pragma mark Getters and setters

  /**
   *  @name status
   *  @fn const ThreadState& status(const size_t& id) const
   *  @brief Querry status of the thread with the given id
   *  @param[in] id  Id of the thread to querry status
   *  @return Const ref to the status of the given thread
   */
  const ThreadState& status(const size_t& id) const;

  /**
   *  @name size
   *  @fn size_t size(void) const
   *  @brief Returns the size (number of threads) of the pool
   */
  size_t size(void) const;

  /**
   *  @name size
   *  @fn size_t size(void) const
   *  @brief Returns the size (number of threads) of the pool
   */
  size_t QueueSize(void) const;

  /**
   *  @name RestartThread
   *  @fn void RestartThread(const size_t& id)
   *  @brief Set the state of a given thread to running
   *  @param[in] id  Id of the thread to restart
   */
  void RestartThread(const size_t& id);

  /**
   *  @name RestartThread
   *  @fn void PauseThread(const size_t& id)
   *  @brief Set the state of a given thread to paused
   *  @param[in] id  Id of the thread to pause
   */
  void PauseThread(const size_t& id);

  /**
   *  @name RestartThread
   *  @fn void StopThread(const size_t& id)
   *  @brief Set the state of a given thread to stopped
   *  @param[in] id  Id of the thread to stop
   */
  void StopThread(const size_t& id);

#pragma mark -
#pragma mark Main methods

  /**
     *  @name
     *  @brief  Add job to the processing queue
     *  @param[in] priority Task's priority
     *  @param[in] f        Function to call
     *  @param[in] args     Function's argument
     *  @return std::future object holding the return value of the task's
     *          function
     */
    template<typename F, typename... Args>
    auto Enqueue(const TaskPriority& priority, F&& f, Args&&... args)
    -> std::future<typename std::result_of<F(Args...)>::type>;

 private:

#pragma mark -
#pragma mark Private attributes and methods

  // Task type
  using Task = std::pair<TaskPriority, std::function<void()>>;

  /**
   *  @struct  TaskComparator
   *  @brief  Functor to sort task by priority
   */
  struct TaskComparator {

    /**
     *  @name   operator()
     *  @fn     bool operator()(const TaskPriority& lhs, const TaskPriority& rhs) const
     *  @brief  Compare two task prioty
     */
    bool operator()(const Task& lhs, const Task& rhs) const {
      return lhs.first > rhs.first;
    }
  };

  // Priority queue type
  using PriorityQueue = std::priority_queue<Task,
                                            std::deque<Task>,
                                            ThreadPool::TaskComparator>;

  /**
   *  @struct ThreadFunctor
   *  @brief  Functor used when instanciating the threads
   */
  struct ThreadFunctor {
    void operator()(ThreadPool* tp, const size_t& i);
  };

  /**
   * @name ThreadPool
   * @fn ThreadPool(const size_t& n_threads)
   * @brief Private constructor of the singleton
   * @param[in] n_threads  Number of threads to create in the pool
   */
  ThreadPool(const size_t& n_threads);

  // Vector of threads (workers)
  std::vector<std::thread> workers_;
  // Queue of tasks
  PriorityQueue queue_;
  // Synchronization
  mutable std::mutex lock_;
  // Conditional variable
  std::condition_variable cond_;
  // Vector of future thread states (vector of commands)
  std::vector<ThreadState> commands_;
  // Vector of threads states
  std::vector<ThreadState> states_;
};

/*
 *  @name
 *  @brief  Add job to the processing queue
 *  @param[in] priority Task's priority
 *  @param[in] f        Function to call
 *  @param[in] args     Function's argument
 *  @return std::future object holding the return value of the task's
 *          function
 */
template<typename F, typename... Args>
auto ThreadPool::Enqueue(const TaskPriority& priority, F&& f, Args&&... args)
-> std::future<typename std::result_of<F(Args...)>::type> {
  // Fcn return type
  using ret_type = typename std::result_of<F(Args...)>::type();

  // Create task
  auto task = std::make_shared<std::packaged_task<ret_type>>(std::bind(std::forward<F>(f),
                                                                       std::forward<Args>(args)...));
  // Define return object
  auto res = task->get_future();
  // Add to queue
  {
    std::unique_lock<std::mutex> lock(this->lock_);
    if (!std::any_of(this->commands_.begin(),
                     this->commands_.end(),
                     [](const ThreadState& s){return s == ThreadState::kRunning;})) {
      throw std::runtime_error("Error, try to add task on a pool where all threads are stopped or paused");
    }
    this->queue_.emplace(priority, [task](void){ (*task)();});
  }
  // Signal new job
  this->cond_.notify_all();
  // Return future object
  return res;
}

std::ostream& operator<< (std::ostream& os, const ThreadPool::ThreadState& state);

#endif /* thread_pool_hpp */
