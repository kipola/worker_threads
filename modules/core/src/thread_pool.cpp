//
//  thread_pool.cpp
//  WorkerThreads
//
//  Created by Gabriel Cuendet on 03.02.18.
//  Copyright © 2018 Gabriel Cuendet. All rights reserved.
//

#include "wk/core/thread_pool.hpp"

#pragma mark -
#pragma mark Initialization methods

/*
 * @name Get
 * @fn static ThreadPool& Get(const size_t& n_threads)
 * @brief Accessor for the thread pool, imlpemented as a singleton
 * @param[in] n_threads  Number of threads to create in the pool, the first
 *            time it is get
 */
ThreadPool& ThreadPool::Get(const size_t& n_threads) {
  // Create a static instance if it does not already exists
  static ThreadPool instance(n_threads);
  return instance;
}

/*
 * @name ~ThreadPool
 * @fn ~ThreadPool(void)
 * @brief Default destructor of the class
 */
ThreadPool::~ThreadPool() {
  // Stop queue
  {
    std::unique_lock<std::mutex> lock(this->lock_);
    for (auto& cmd : commands_) {
      cmd = ThreadState::kStopped;
    }
  }
  // Signal all waiting thread to run
  this->cond_.notify_all();
  // Wait until all threads are done
  for (std::thread& thr : this->workers_) {
    thr.join();
  }
}

#pragma mark -
#pragma mark Getters and setters

/*
 *  @name status
 *  @fn const ThreadState& status(const size_t& id) const
 *  @brief Querry status of the thread with the given id
 *  @param[in] id  Id of the thread to querry status
 *  @return Const ref to the status of the given thread
 */
const ThreadPool::ThreadState& ThreadPool::status(const size_t& id) const {
  std::unique_lock<std::mutex> lock(this->lock_);
  return states_[id];
}

/*
 *  @name size
 *  @fn size_t size(void) const
 *  @brief Returns the size (number of threads) of the pool
 */
size_t ThreadPool::size(void) const {
  std::unique_lock<std::mutex> lock(this->lock_);
  return workers_.size();
}

/**
 *  @name QueueSize
 *  @fn size_t sQueueSizeize(void) const
 *  @brief Returns the queue size (number of queued functions)
 */
size_t ThreadPool::QueueSize(void) const {
  std::unique_lock<std::mutex> lock(this->lock_);
  return queue_.size();
}

/*
 *  @name RestartThread
 *  @fn void RestartThread(const size_t& id)
 *  @brief Set the state of a given thread to running
 *  @param[in] id  Id of the thread to restart
 */
void ThreadPool::RestartThread(const size_t& id) {
  {
    std::unique_lock<std::mutex> lock(this->lock_);
    if (states_[id] == ThreadState::kStopped) {
      std::cerr << "Cannot restart a stopped thread!" << std::endl;    }
    commands_[id] = ThreadState::kRunning;
  }
  this->cond_.notify_all();
}

/*
 *  @name PauseThread
 *  @fn void PauseThread(const size_t& id)
 *  @brief Set the state of a given thread to paused
 *  @param[in] id  Id of the thread to pause
 */
void ThreadPool::PauseThread(const size_t& id) {
  {
    std::unique_lock<std::mutex> lock(this->lock_);
    if (states_[id] == ThreadState::kStopped) {
      std::cerr << "Cannot pause a stopped thread!" << std::endl;
    }
    this->commands_[id] = ThreadState::kPaused;
  }
  this->cond_.notify_all();
}

/*
 *  @name StopThread
 *  @fn void StopThread(const size_t& id)
 *  @brief Set the state of a given thread to stopped
 *  @param[in] id  Id of the thread to stop
 */
void ThreadPool::StopThread(const size_t& id) {
  {
    std::unique_lock<std::mutex> lock(this->lock_);
    commands_[id] = ThreadState::kStopped;
  }
  this->cond_.notify_all();
}

#pragma mark -
#pragma mark Private attributes and methods

/*
 *  @struct ThreadFunctor
 *  @brief  Functor used when instanciating the threads
 */
void ThreadPool::ThreadFunctor::operator()(ThreadPool* tp, const size_t& i) {
  // Loop forever
  while(true) {
    // Get task
    std::function<void()> task;
    // Wait till some tasks are pending and the thread state is running, or if we stop
    {
      std::unique_lock<std::mutex> lock(tp->lock_);
      if (tp->commands_[i] == ThreadState::kRunning) {
        tp->states_[i] = ThreadState::kWaiting;
      }

      tp->cond_.wait(lock, [tp, i](void) {
        return tp->commands_[i] == ThreadState::kStopped ||
        !tp->queue_.empty() ||
        tp->commands_[i] == ThreadState::kPaused;
      });
      // Stopping ?
      if (tp->commands_[i] == ThreadState::kStopped && tp->queue_.empty()) {
        tp->states_[i] = ThreadState::kStopped;
        return;
      }

      // Paused ?
      if (tp->commands_[i] == ThreadState::kPaused) {
        tp->states_[i] = ThreadState::kPaused;
        continue;
      }

      // Queue not empty and thread running, pick pending job
      task = std::move(tp->queue_.top().second);
      tp->queue_.pop();
      tp->states_[i] = ThreadState::kRunning;
    }
    // Execute task
    task();
  }
}

/*
 * @name ThreadPool
 * @fn ThreadPool(const size_t& n_threads)
 * @brief Private constructor of the singleton
 * @param[in] n_threads  Number of threads to create in the pool
 */
ThreadPool::ThreadPool(const size_t& n_threads) {
  std::cout << "ThreadPool::ThreadPool(" << n_threads << ")" << std::endl;
  // Initialize each worker (n_threads of them) with fn
  for (size_t i = 0; i < n_threads; ++i) {
    {
      std::unique_lock<std::mutex> lock(this->lock_);
      this->commands_.emplace_back(ThreadState::kRunning);
      this->states_.emplace_back(ThreadState::kWaiting);
    }

    ThreadFunctor fn;
    workers_.emplace_back(fn, this, i);
  }
}

std::ostream& operator<< (std::ostream& os, const ThreadPool::ThreadState& state) {
  switch (state) {
    case ThreadPool::ThreadState::kStopped:
      os << "stopped";
      break;
    case ThreadPool::ThreadState::kPaused:
      os << "paused";
      break;
    case ThreadPool::ThreadState::kWaiting:
      os << "waiting";
      break;
    case ThreadPool::ThreadState::kRunning:
      os << "running";
      break;
    default:
      break;
  }

  return os;
}
